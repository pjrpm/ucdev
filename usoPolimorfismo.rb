class ApiConnector
	def initialize(title:, description:, url: "google.com")
		@title = title
		@description = description
		@url = url
	end
	def to_s
		"CONNECTOR: #{@title}, description: #{@description}, url: #{@url}"
	end

	def secret_method
		puts "Secret stuff...from #{@title} with #{@description} "
	end

	def connector_logger
		puts "Starting API Logger"
	end

private  :secret_method
end

class SmsConnector < ApiConnector
	def send_sms
		puts "Sending SMS message...\n #{@title} \n #{@description}"
	end
	def connector_logger
		puts "Starting API Logger for SMS for SMS Connector"
	end

end

llamadaApi = ApiConnector.new(title:"Conecta a la API", description: "Primera conexion")
llamadaSms = SmsConnector.new(title:"Conecta a la API de SMS", description: "Conexion mensajeria")
puts llamadaApi
puts llamadaSms
llamadaSms.connector_logger

