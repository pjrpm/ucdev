class Factura
attr_accessor :secuencia, :cliente, :total
	def initialize(secuencia,cliente, total)
     @cliente = cliente
	 @total = Float(total)
	 @secuencia = secuencia
	end

	def to_s
        "********\nFactura No. #{@secuencia}\nCliente: #{@cliente}\nTotal a pagar $: #{@total}"
	end

end

facturaA = Factura.new("000250","Pedro Castillo",150.35)
facturaB = Factura.new("000251","Rubby Perez",175.53)
puts facturaA
puts facturaB
