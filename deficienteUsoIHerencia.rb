class SmsConnector
	def initialize(title, description, url = "google.com")
		@title = title
		@description = description
		@url = url
	end
	def send_sms
		puts "Sending SMS message"
	end
end

class MailerConnector
	def initialize(title, description, url = "google.com")
		@title = title
		@description = description
		@url = url
	end
	def send_mail
		puts "Sending Mail message"
	end
end

class PhoneConnector
	def initialize(title, description, url = "google.com")
		@title = title
		@description = description
		@url = url
	end
	def place_call
		puts "Placing phone call"
	end
end

class XyzConnector
	def initialize(title, description, url = "google.com")
		@title = title
		@description = description
		@url = url
	end

	def does_something_else
		puts "Secret stuff..."
	end
end

